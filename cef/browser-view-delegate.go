//----------------------------------------
//
// Copyright © yanghy. All Rights Reserved.
//
// Licensed under Apache License Version 2.0, January 2004
//
// https://www.apache.org/licenses/LICENSE-2.0
//
//----------------------------------------

package cef

import "unsafe"

// ICefBrowserViewDelegate TODO 未实现
type ICefBrowserViewDelegate struct {
	instance unsafe.Pointer
}
