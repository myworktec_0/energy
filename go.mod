module github.com/energye/energy/v2

go 1.18

require (
	github.com/energye/golcl v0.0.0-20230221030031-f70c394ea7a4
	github.com/godbus/dbus/v5 v5.1.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/json-iterator/go v1.1.12
	github.com/tevino/abool v0.0.0-20220530134649-2bfc934cb23c
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
